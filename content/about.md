+++
title = "whoami"
slug = "whoami"
+++

* Application Security Engineer @ Facebook
* OWASP Czech Chapter Lead
* Master's in Cyber Security @ Masaryk University, Brno
* Occasional [CTF player](https://ctftime.org/team/53880)
* Even more occasional [bug bounty hunter](https://hackerone.com/janmasarik)
* Subpar [Python dev](https://github.com/janmasarik)
* Filtered Coffee Lover
* OSCP

## Contact

* Email: 
```python -c "import codecs; print(codecs.decode('wna.znfnevx9@tznvy.pbz', 'rot13'))"```

* [PGP Key](https://keybase.io/sl4ve/pgp_keys.asc?fingerprint=b7b3f38ace9133469de4afea5e022c334c9dc60f)

* [Keybase](https://keybase.io/sl4ve)
