---
title: "DEF CON Quals 2018 - Easy Pisy (web, crypto) 🍡"
date: 2018-05-14T08:45:06+02:00
---

![](https://i.imgur.com/KZZDfU5.png)

[Challenge source code](https://gist.github.com/janmasarik/232381eec3918313b5b4d2c20ca1ed0f)

### TLDR

- php `openssl_verify` uses SHA1 as default signing algorithm
- generate PDFs with different content but identical SHA1 hash with [sha1collider](https://github.com/nneonneo/sha1collider)

### openssl\_verify

Crypto category of challenge tells us that this will probably be crypto vuln ^_^. After some lurking, we checked php docs for [openssl_verify](https://secure.php.net/manual/en/function.openssl-verify.php) and noticed, that it uses SHA1 as a default signature algorithm. [SHA1 Collision attack](https://security.googleblog.com/2017/02/announcing-first-sha1-collision.html) was released last year.

### sha1collider
Quick google search showed us python [sha1collider](https://github.com/nneonneo/sha1collider) which we used to generate `pdf` files with same SHA1 hash:

```bash
$ python3 sha1collider/collide.py samples/echo-ciao.pdf samples/execute-ls.pdf --progressive
```

![](https://i.imgur.com/hxFJKn0.png)

![](https://i.imgur.com/EfWcO62.png)

We see that text from "shattered" pdf wasn't parsed properly. There is a mention in README of `sha1collider` about it:

> However, some PDF renderers, such as my version of GhostScript, cannot parse the resulting JPEG correctly

About `--progressive` flag:

> Progressive mode works with many smaller PDFs (at lower resolution, for example), but breaks down with larger images. However, it produces PDFs that are broadly compatible because it does not involve bending the JPEG spec. This is the mode used by Google+CWI in generating their own PoC PDF pair.
https://github.com/nneonneo/sha1collider#remarks

Final command:

```bash
$ python3 sha1collider/collide.py samples/echo-ciao.pdf samples/execute-ls.pdf --progressive
```
![](https://i.imgur.com/dY962WR.png)

Now lets send `EXECUTE ls` with signature of `ECHO ciao` pdf which we just generated.

![](https://i.imgur.com/uCZ5Sbd.png)

Now we just need to generate 2 pdfs (using Pages ¯\\\_(ツ)_/¯), run sha1collider on them, generate signature of `ECHO ciao` and submit `EXECUTE cat flag`.

![](https://i.imgur.com/9grUW7z.png)

Easy Pisy.
`OOO{phP_4lw4y5_d3l1v3r5_3h7_b35T_fl4g5}`


